# What Tags

`what-tags` is a little tool for use in CI environments that calculates Docker tags to apply to an image based on the git information of the commit being built.

- It will always produce a tag that is the SHA of the current commit, truncated to 8 characters.
- If any branches point to that commit, their names will be produced.  If any of those branches are remote, the remote name will be stripped from the beginning.
- If we are on a git-tagged commit, it also produces a matching docker tag.
- If the `--semver=foo` flag is provided, it examines all semver-y git tags to see if the version provided to `--semver` is the latest of a given `major` or `major.minor` "lineage". If so, it produces `major` or `major.minor` tags.  If it's the latest of ALL versions, it produces the tag `latest`.

All tags produced will be slugified for use in docker - only lower-case alphanumerics, dashes, and periods, and at most 128 characters.

## Usage
In your gitlab CI, for instance:
```sh
export REGISTRY=docker-registry.example.com
# Build the docker image you want to distribute
docker build --tag dummyapp:build .
# Apply what-tags produced docker tags to that image
what-tags --semver="${CI_COMMIT_TAG}" | xargs -I{} docker tag dummyapp:build $REGISTRY/dummyapp:{}
# Push the resultant tags to your registry
what-tags --semver="${CI_COMMIT_TAG}" | xargs -I{} docker push $REGISTRY/dummyapp:{}
```
