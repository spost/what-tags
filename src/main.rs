use clap::{crate_version, App, Arg};
use git2::{BranchType, Repository};
use std::collections::{BTreeSet, HashMap};
use std::str;
use itertools::Itertools;

fn get_docker_tags(
    head: String,
    head_branches: Vec<String>,
    head_tags: Vec<String>,
    all_tags: Vec<String>,
    provided_semver: Option<String>,
) -> Vec<String> {
    let mut docker_tags = head_branches;

    // first 8 characters of the sha as a string
    // this is safe because there are no non-ASCII characters in SHAs
    // if that changes let me know
    docker_tags.push(String::from(&head[0..8]));

    if let Some(provided_semver) = provided_semver {
        if let Ok(parsed_semver) = lenient_semver::parse(&provided_semver) {
            let mut semversion_tags: Vec<_> = all_tags
                .iter()
                .flat_map(|tag| lenient_semver::parse(tag))
                .collect();
            semversion_tags.push(parsed_semver.clone());
            semversion_tags.sort();

            docker_tags.push(parsed_semver.to_string());

            if let Some(highest_major_match) = semversion_tags
                .iter()
                .filter(|tag| tag.major == parsed_semver.major)
                .last()
            {
                if highest_major_match == &parsed_semver {
                    docker_tags.push(parsed_semver.major.to_string())
                }
            }

            if let Some(highest_minor_match) = semversion_tags
                .iter()
                .filter(|tag| tag.major == parsed_semver.major && tag.minor == parsed_semver.minor)
                .last()
            {
                if highest_minor_match == &parsed_semver {
                    docker_tags.push(format!("{}.{}", parsed_semver.major, parsed_semver.minor))
                }
            }

            if let Some(highest_version) = semversion_tags.iter().last() {
                if highest_version == &parsed_semver {
                    docker_tags.push(String::from("latest"))
                }
            }
        }
    }

    for head_tag in head_tags {
        assert!(all_tags.contains(&head_tag));
        if lenient_semver::parse(&head_tag).is_ok() {
            // this tag is a semver - we're going to ignore it because we only want to use the
            // explicitly-provided semver.
        } else {
            docker_tags.push(head_tag.clone());
        }
    }

    return docker_tags.iter().map(slugify).unique().collect();
}

fn slugify(input: &String) -> String {
    let lowercased = if input.len() > 128 {
        input.to_ascii_lowercase()[..128].to_string()
    } else {
        input.to_ascii_lowercase()[..].to_string()
    };
    lowercased
        .replace(|c: char| !c.is_alphanumeric() && c != '.' && c != '-', "-")
        .trim_end_matches('-')
        .to_string()
}

fn main() {
    let args = App::new("what-tags")
        .version(crate_version!())
        .author("Sam Osterkil")
        .about("Builds docker tags from git information")
        .arg(
            Arg::with_name("repo_root")
                .long("repo")
                .takes_value(true)
                .help(
                    "The root directory of the git repo to analyze.  Default: current directory.",
                ),
        )
        .arg(
            Arg::with_name("semver")
                .long("semver")
                .takes_value(true)
                .help("A semver for this build.  Default: none. Ignored if empty string."),
        )
        .get_matches();


    let semver: Option<String> = args.value_of("semver").and_then(|v| {
        if v.is_empty() {None} else {Some(v.to_string())}
    });

    let loc = args.value_of("repo_root").unwrap_or(".");
    let repo = Repository::open(loc).unwrap_or_else(|_| panic!("Could not open repo at {}", loc));

    let head = repo
        .head()
        .expect("Could not find HEAD?")
        .peel_to_commit()
        .expect("HEAD was not a commit.  You're on your own here, boss.");

    let head_branches: Vec<_> = repo
        .branches(None)
        .expect("Could not get a branch list?")
        // unwrap Result<Branch, BranchType, Error>
        .flatten()
        .filter_map(|(branch, btype)| {
            branch.get().peel_to_commit().map_or(None, |branch_commit| {
                if branch_commit.id() != head.id() {
                    None
                } else {
                    let full_branch_name = branch.name().unwrap().unwrap().to_string();
                    let branch_name = match btype {
                        // If the branch is local, we good
                        BranchType::Local => full_branch_name,
                        // If it's remote, trim off the remote
                        BranchType::Remote => {
                            full_branch_name.split_once("/").unwrap().1.to_string()
                        }
                    };
                    // HEAD will always point to HEAD, obviously - we don't want it
                    if branch_name.to_ascii_lowercase().as_str() == "head" {
                        None
                    } else {
                        Some(branch_name)
                    }
                }
            })
        })
        .collect::<BTreeSet<String>>() // collect into a BTreeSet to sort and unique.
        .into_iter()
        .collect(); // Collect back into a vector to accommodate the rest of the API.

    let mut all_tags = HashMap::new();
    let _ = repo.tag_foreach(|id, name| {
        all_tags.insert(
            str::from_utf8(name)
                .unwrap()
                .to_owned()
                .replace("refs/tags/", ""),
            id.to_owned(),
        );
        true
    });

    let head_tags: Vec<String> = all_tags
        .iter()
        .filter_map(|i| {
            let tag_id = i.1; // This will point to a tag object if it's an annotated tag, or a commit if it's not
            let commit_id = repo
                .find_tag(*tag_id)
                .map_or(tag_id.to_owned(), |tag| tag.peel().unwrap().id());
            if commit_id == head.id() {
                Some(i.0.to_owned())
            } else {
                None
            }
        })
        .collect();

    for tag in get_docker_tags(
        head.id().to_string(),
        head_branches,
        head_tags,
        all_tags.keys().map(|k| k.to_owned()).collect::<Vec<_>>(),
        semver
    ) {
        println!("{}", tag)
    }
}

#[cfg(test)]
mod tests {
    use spectral::prelude::*;

    #[test]
    fn when_given_boring_values_for_all_inputs_they_are_all_in_the_output() {
        let results = crate::get_docker_tags(
            "da54030c296e52ad7bc2a55af547150241f932e2".to_string(),
            vec!["boring-branch".to_string()],
            vec!["boring-tag".to_string()],
            vec!["boring-tag".to_string()],
            None
        );
        assert_that!(&results).contains(&"da54030c".to_string());
        assert_that!(&results).contains(&"boring-tag".to_string());
        assert_that!(&results).contains(&"boring-branch".to_string());
    }

    #[test]
    fn when_the_branch_name_contains_special_and_uppercased_characters_it_is_slugified_in_the_output(
    ) {
        let results = crate::get_docker_tags(
            "da54030c296e52ad7bc2a55af547150241f932e2".to_string(),
            vec!["Branch$With!Shenanigans@".to_string()],
            vec![],
            vec![],
            None
        );
        assert_that!(&results).contains(&"branch-with-shenanigans".to_string());
    }

    #[test]
    fn when_we_are_not_on_a_branch_or_tagged_commit_we_only_get_the_commit_sha() {
        let results = crate::get_docker_tags(
            "da54030c296e52ad7bc2a55af547150241f932e2".to_string(),
            vec![],
            vec![],
            vec![],
            None
        );
        assert_that!(&results).contains(&"da54030c".to_string());
        assert_that!(&results).has_length(1);
    }

    #[test]
    fn when_the_branch_name_is_more_than_128_characters_it_is_truncated() {
        let results = crate::get_docker_tags(
            "da54030c296e52ad7bc2a55af547150241f932e2".to_string(),
            vec!["a".repeat(129)],
            vec![],
            vec![],
            None
        );
        assert_that!(&results).contains("a".repeat(128));
    }

    #[test]
    fn when_there_is_one_semver_tag_and_it_is_not_provided_explicitly_it_is_not_in_the_output() {
        let results = crate::get_docker_tags(
            "da54030c296e52ad7bc2a55af547150241f932e2".to_string(),
            vec![],
            vec![String::from("1.2.3")],
            vec![String::from("1.2.3")],
            None
        );
        assert_that!(&results).does_not_contain(&String::from("1.2.3"));
    }

    #[test]
    fn when_there_is_one_semver_tag_and_it_is_also_the_provided_semver_it_and_its_components_are_in_the_output() {
        let results = crate::get_docker_tags(
            "da54030c296e52ad7bc2a55af547150241f932e2".to_string(),
            vec![],
            vec![String::from("1.2.3")],
            vec![String::from("1.2.3")],
            Some("1.2.3".to_string())
        );
        assert_that!(&results).contains_all_of(&vec![
            &String::from("1"),
            &String::from("1.2"),
            &String::from("1.2.3"),
        ]);
    }

    #[test]
    fn when_there_are_multiple_1_x_y_tags_and_head_is_the_latest_of_them_1_is_in_the_output() {
        let results = crate::get_docker_tags(
            "da54030c296e52ad7bc2a55af547150241f932e2".to_string(),
            vec![],
            vec![String::from("1.2.3")],
            vec![
                String::from("1.0.0"),
                String::from("1.1.1"),
                String::from("1.2.3"),
            ],
            Some("1.2.3".to_string())
        );
        assert_that!(&results).contains_all_of(&vec![
            &String::from("1"),
            &String::from("1.2"),
            &String::from("1.2.3"),
        ]);
    }

    #[test]
    fn when_there_are_multiple_1_x_y_tags_and_head_is_not_the_latest_of_them_1_is_not_in_the_output(
    ) {
        let results = crate::get_docker_tags(
            "da54030c296e52ad7bc2a55af547150241f932e2".to_string(),
            vec![],
            vec![String::from("1.2.3")],
            vec![
                String::from("1.0.0"),
                String::from("1.1.1"),
                String::from("1.2.3"),
                String::from("1.4.6"),
            ],
            Some("1.2.3".to_string())
        );
        assert_that!(&results).contains_all_of(&vec![&String::from("1.2"), &String::from("1.2.3")]);
        assert_that!(&results).does_not_contain(&String::from("1"));
    }

    #[test]
    fn lenient_semver_can_parse_v_prefixed_semvers() {
        assert_that!(lenient_semver::parse("v1.2.3"))
            .is_ok()
            .is_equal_to(semver::Version::new(1, 2, 3))
    }

    #[test]
    fn v_prefixed_versions_are_okay() {
        let results = crate::get_docker_tags(
            "da54030c296e52ad7bc2a55af547150241f932e2".to_string(),
            vec![],
            vec![String::from("v1.2.3")],
            vec![String::from("v1.2.3")],
            Some("v1.2.3".to_string())
        );
        assert_that!(&results).contains_all_of(&vec![
            &String::from("1"),
            &String::from("1.2"),
            &String::from("1.2.3"),
        ]);
    }

    #[test]
    fn versions_with_a_two_part_release_as_the_prerelease_component_work() {
        let results = crate::get_docker_tags(
            "da54030c296e52ad7bc2a55af547150241f932e2".to_string(),
            vec![],
            vec![String::from("1.0.1-v1.1.2")],
            vec![String::from("1.0.1-v1.1.2")],
            Some("1.0.1-v1.1.2".to_string())
        );
        assert_that!(&results).contains_all_of(&vec![
            &String::from("1"),
            &String::from("1.0"),
            &String::from("1.0.1-v1.1.2"),
        ]);
    }
}
