#/bin/sh
set -euo pipefail

WORKDIR=$(mktemp -d -p $(pwd))

cleanup() {
  rm -rf $WORKDIR
}
trap cleanup EXIT

compare() {
  echo $1 | tr ' ' '\n' | sort > expected
  echo $2 | tr ' ' '\n' | sort > actual
  diff=$(comm -3 expected actual)
  if test -n "$diff"; then
    echo "Unexpected difference:"
    echo $diff
    return 1
  else
    echo pass
    return 0
  fi
}

headshort() {
  git rev-parse HEAD | head -c 8
}

# Copy what-tags to the test working directory
cp what-tags $WORKDIR/what-tags

# make a git repo to run what-tags in so we can check its output
BRANCH_NAME=main
cd $WORKDIR && git init . 
git config user.email "whattagstest@example.com"
git config user.name "what tags test"
git switch -c $BRANCH_NAME

git commit --allow-empty --message "Version 1.0.0"
git tag --annotate --message "Version 1.0.0" 1.0.0 HEAD

git commit --allow-empty --message "Version 1.0.1"
git tag --annotate --message "Version 1.0.1" 1.0.1 HEAD

git commit --allow-empty --message "Version 1.0.2"
git tag --annotate --message "Version 1.0.2" 1.0.2 HEAD

git commit --allow-empty --message "Version 1.1.0"
git tag --annotate --message "Version 1.1.0" 1.1.0 HEAD

git commit --allow-empty --message "Version 1.1.1"
git tag --annotate --message "Version 1.1.1" 1.1.1 HEAD

git commit --allow-empty --message "Version 1.1.2"
git tag --annotate --message "Version 1.1.2" 1.1.2 HEAD

git commit --allow-empty --message "Version 1.1.3"
git tag --annotate --message "Version 1.1.3" 1.1.3 HEAD

git commit --allow-empty --message "Version 1.2.0"
git tag --annotate --message "Version 1.2.0" 1.2.0 HEAD

git commit --allow-empty --message "Version 2.0.0"
git tag --annotate --message "Version 2.0.0" 2.0.0 HEAD

git commit --allow-empty --message "Version 2.5.0"
git tag --annotate --message "Version 2.5.0" 2.5.0 HEAD

git commit --allow-empty --message "Version 2.5.1"
git tag --annotate --message "Version 2.5.1" 2.5.1 HEAD

git commit --allow-empty --message "Not a version"

echo Setup Complete
echo
pwd
ls
./what-tags
echo Testing non-version commit
compare "$(headshort) ${BRANCH_NAME}" "$(./what-tags)"

echo Testing 1.0.0
git checkout 1.0.0 2> /dev/null
compare "$(headshort)  1.0.0" "$(./what-tags --semver 1.0.0)"

echo Testing 1.0.1
git checkout 1.0.1 2> /dev/null
compare "$(headshort) 1.0.1" "$(./what-tags --semver 1.0.1)"

echo Testing 1.0.2
git checkout 1.0.2 2> /dev/null
compare "$(headshort) 1.0.2 1.0" "$(./what-tags --semver 1.0.2)"

echo Testing 1.1.0
git checkout 1.1.0 2> /dev/null
compare "$(headshort) 1.1.0" "$(./what-tags --semver 1.1.0)"

echo Testing 1.1.1
git checkout 1.1.1 2> /dev/null
compare "$(headshort) 1.1.1" "$(./what-tags --semver 1.1.1)"

echo Testing 1.1.2
git checkout 1.1.2 2> /dev/null
compare "$(headshort) 1.1.2" "$(./what-tags --semver 1.1.2)"

echo Testing 1.1.3
git checkout 1.1.3 2> /dev/null
compare "$(headshort) 1.1.3 1.1" "$(./what-tags --semver 1.1.3)"

echo Testing 1.2.0
git checkout 1.2.0 2> /dev/null
compare "$(headshort) 1.2.0 1.2 1" "$(./what-tags --semver 1.2.0)"

echo Testing 2.0.0
git checkout 2.0.0 2> /dev/null
compare "$(headshort) 2.0.0 2.0" "$(./what-tags --semver 2.0.0)"

echo Testing 2.5.0
git checkout 2.5.0 2> /dev/null
compare "$(headshort) 2.5.0" "$(./what-tags --semver 2.5.0)"

echo Testing 2.5.1
git checkout 2.5.1 2> /dev/null
compare "$(headshort) 2.5.1 2.5 2 latest" "$(./what-tags --semver 2.5.1)"

echo Testing unannotated 2.5.2
git checkout main 2> /dev/null
git commit --allow-empty --message "Unannotated Version 2.5.2"
git tag 2.5.2 HEAD
compare "$(headshort) ${BRANCH_NAME} 2.5.2 2.5 2 latest" "$(./what-tags --semver 2.5.2)"

echo Testing multiple unannotated tags
git commit --allow-empty --message "Unannotated Version 2.5.3"
git tag 2.5.3 HEAD
git commit --allow-empty --message "Unannotated Version 2.6.0"
git tag 2.6.0 HEAD
compare "$(headshort) ${BRANCH_NAME} 2.6.0 2.6 2 latest" "$(./what-tags --semver 2.6.0)"
git checkout 2.5.3 2> /dev/null
compare "$(headshort) 2.5.3 2.5" "$(./what-tags --semver 2.5.3)"

echo Testing tags with a v
git checkout main 2> /dev/null
git commit --allow-empty --message "Unannotated Version v3.0.0"
git tag v3.0.0 HEAD
compare "$(headshort) ${BRANCH_NAME} 3.0.0 3.0 3 latest" "$(./what-tags --semver v3.0.0)"

echo Testing that when tags are not provided there is no version output
compare "$(headshort) ${BRANCH_NAME}" "$(./what-tags)"

echo Testing that when a tag is provided but there is no tag, it is still in the output
compare "$(headshort) ${BRANCH_NAME} 3.0.1 3.0 3 latest" "$(./what-tags --semver 3.0.1)"
